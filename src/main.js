import { createApp } from 'vue'
import App from './App.vue'

import router from "./router"

const app = createApp(App)

import { dialogInstall } from './components/Modal';

app.use(dialogInstall).use(router).mount('#app')